//
//  UserInfoCollectionViewCell.swift
//  InstaLikeApp
//
//  Created by Bonnet Nicolas on 16.10.18.
//  Copyright © 2018 NicolasBonnet. All rights reserved.
//

import SDWebImage
import UIKit

final class UserInfoCollectionViewCell: UICollectionViewCell, UserManagerInjector {
    
    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            imageView.layer.masksToBounds = true
            imageView.layer.borderColor = UIColor.black.withAlphaComponent(0.3).cgColor
            imageView.layer.borderWidth = 0.5
        }
    }
    @IBOutlet weak var postsNumberLabel: UILabel!
    @IBOutlet weak var postsLabel: UILabel! {
        didSet {
            postsLabel.text = R.string.localizable.profilePostsLabel()
        }
    }
    @IBOutlet weak var followersNumberLabel: UILabel!
    @IBOutlet weak var followersLabel: UILabel! {
        didSet {
            followersLabel.text = R.string.localizable.profileFollowersLabel()
        }
    }
    @IBOutlet weak var followingNumberLabel: UILabel!
    @IBOutlet weak var followingLabel: UILabel! {
        didSet {
            followingLabel.text = R.string.localizable.profileFollowingLabel()
        }
    }
    @IBOutlet weak var logoutButton: UIButton! {
        didSet {
            logoutButton.setTitle(R.string.localizable.profileLogout(), for: .normal)
            logoutButton.layer.borderColor = UIColor.black.withAlphaComponent(0.3).cgColor
            logoutButton.layer.borderWidth = 1.0
            logoutButton.layer.cornerRadius = 4
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        imageView.layer.cornerRadius = imageView.frame.width/2
    }
    
    @IBAction func logout(_ sender: Any) {
        self.userManager.logout()
    }
    
    func updateUI(userInfo: UserInfoSection) {
        imageView?.sd_setImage(with: URL(string: userInfo.imageUrlString ?? ""), completed: nil)
        postsNumberLabel.text = R.string.localizable.profileEmptyNumber(value: Int(userInfo.numberOfPost ?? 0))
        followersNumberLabel.text = R.string.localizable.profileEmptyNumber(value: Int(userInfo.numberOfFollowers ?? 0))
        followingNumberLabel.text = R.string.localizable.profileEmptyNumber(value: Int(userInfo.numberOfFollowing ?? 0))
    }
}
