//
//  ImageCollectionViewCell.swift
//  InstaLikeApp
//
//  Created by Bonnet Nicolas on 21.10.18.
//  Copyright © 2018 NicolasBonnet. All rights reserved.
//

import UIKit
import SDWebImage

final class ImageCollectionViewCell: UICollectionViewCell {
    var media: Media!
    var customRatio: CGFloat? {
        didSet {
            updateFrameImageView()
        }
    }
    
    let imageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        view.backgroundColor = UIColor(white: 0.95, alpha: 1)
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(imageView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateFrameImageView()
    }
    
    override func prepareForReuse() {
        imageView.image = nil
    }
    
    private func updateFrameImageView() {
        if customRatio != nil {
            let cellRatio: CGFloat = bounds.size.height / bounds.size.width
            if cellRatio > customRatio! {
                let imageHeight = bounds.size.width * customRatio!
                imageView.frame = CGRect(x: 0, y: (bounds.size.height - imageHeight) / 2.0, width: bounds.size.width, height: bounds.size.width * customRatio!)
            } else {
                let imageWidth = bounds.size.height * customRatio!
                imageView.frame = CGRect(x: (bounds.size.width - imageWidth) / 2.0, y: 0, width: imageWidth, height: bounds.size.height)
            }
        } else {
            imageView.frame = contentView.bounds
        }
    }
    
    func updateUI(media: Media, quality: ImageQuality = .original, contentMode: UIView.ContentMode = .scaleAspectFill, customRatio: CGFloat? = nil) {
        self.media = media
        self.customRatio = customRatio
        imageView.contentMode = contentMode
        imageView.setImageWith(imageBlock: media.images, quality: quality)
    }
}
