//
//  FancyView.swift
//  InstaLikeApp
//
//  Created by Bonnet Nicolas on 14.10.18.
//  Copyright © 2018 NicolasBonnet. All rights reserved.
//

import UIKit

open class FancyView: UIView {
    
    private struct Animation {
        static let keyPath = "colors"
        static let key = "ColorChange"
        static let duration = 4.0
    }
    
    fileprivate let gradient = CAGradientLayer()
    private let possibleColors: [UIColor] = [Colors.BlueLight,
                                             Colors.BlueDark,
                                             Colors.Purple,
                                             Colors.Red,
                                             Colors.Orange,
                                             Colors.Yellow,
                                             Colors.GreenLight]
    
    var currentGradient: Int = 0
    var _gradientSet: [[CGColor]] = []
    var gradientSet: [[CGColor]] {
        get {
            if _gradientSet.count > 0 {
                return _gradientSet
            }
            guard possibleColors.count > 0 else { return [] }
            for i in 0...possibleColors.count {
                _gradientSet.append([possibleColors[i % possibleColors.count].cgColor,
                               possibleColors[(i + 1) % possibleColors.count].cgColor])
            }
            return _gradientSet
        }
    }
    var currentGradientSet: [CGColor] {
        get {
            return gradientSet[currentGradient % gradientSet.count]
        }
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        gradient.frame = bounds
    }
    
    open override func removeFromSuperview() {
        super.removeFromSuperview()
        gradient.removeAllAnimations()
        gradient.removeFromSuperlayer()
    }
    
    public func startAnimation() {
        gradient.removeAllAnimations()
        setup()
        animateGradient()
    }
    
    public func setup() {
        gradient.frame = bounds
        gradient.colors = currentGradientSet
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.0)
        gradient.drawsAsynchronously = true
        layer.insertSublayer(gradient, at: 0)
    }
    
    func animateGradient() {
        currentGradient += 1
        let animation = CABasicAnimation(keyPath: Animation.keyPath)
        animation.duration = Animation.duration
        animation.toValue = currentGradientSet
        animation.fillMode = .forwards
        animation.isRemovedOnCompletion = false
        animation.delegate = self
        gradient.add(animation, forKey: Animation.key)
    }
}

extension FancyView: CAAnimationDelegate {
    public func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if flag {
            gradient.colors = currentGradientSet
            animateGradient()
        }
    }
}
