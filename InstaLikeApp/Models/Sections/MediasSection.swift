//
//  MediaModel.swift
//  InstaLikeApp
//
//  Created by Bonnet Nicolas on 21.10.18.
//  Copyright © 2018 NicolasBonnet. All rights reserved.
//

import IGListKit

class MediasSection {
    var indentifer: String!
    let medias: [Media]!
    
    init(_ someMedias: [Media]?) {
        medias = someMedias
    }
    
    func identifier() -> String {
        if indentifer == nil {
            indentifer = medias?.map{ $0.id ?? "" }.joined() ?? ""
        }
        return indentifer
    }
}

extension MediasSection: ListDiffable {
    func diffIdentifier() -> NSObjectProtocol {
        return identifier() as NSObjectProtocol
    }
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        if let object = object as? MediasSection {
            return identifier() == object.identifier()
        }
        return false
    }
}
