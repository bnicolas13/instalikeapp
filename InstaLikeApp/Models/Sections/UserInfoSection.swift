//
//  UserInfoSection.swift
//  InstaLikeApp
//
//  Created by Bonnet Nicolas on 17.10.18.
//  Copyright © 2018 NicolasBonnet. All rights reserved.
//

import IGListKit

class UserInfoSection {
    var identifier: String
    var imageUrlString: String!
    var numberOfPost: Int32!
    var numberOfFollowers: Int32!
    var numberOfFollowing: Int32!
    
    init(id: String, user: ProfileUser?) {
        identifier = id + (user?.username ?? "")
        imageUrlString = user?.profilePicture
        numberOfPost = user?.counts?.posts
        numberOfFollowers = user?.counts?.followers
        numberOfFollowing = user?.counts?.following
    }
}

extension UserInfoSection: ListDiffable {
    func diffIdentifier() -> NSObjectProtocol {
        return identifier as NSObjectProtocol
    }
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        if let object = object as? UserInfoSection {
            return identifier == object.identifier &&
                numberOfPost == object.numberOfPost &&
                imageUrlString == object.imageUrlString &&
                numberOfFollowers == object.numberOfFollowers &&
                numberOfFollowing == object.numberOfFollowing
        }
        return false
    }
}
