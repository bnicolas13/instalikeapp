//
//  UserManager.swift
//  InstaLikeApp
//
//  Created by Bonnet Nicolas on 17.10.18.
//  Copyright © 2018 NicolasBonnet. All rights reserved.
//

import Alamofire
import AlamofireCoreData
import CoreData
import UIKit
import WebKit

class UserManager {
    
    public struct Instagram {
        static let baseUrl = "https://api.instagram.com/"
        static let AuthUrl = Instagram.baseUrl + "oauth/authorize/"
        static let AccessTokenUrl = Instagram.baseUrl + "oauth/access_token/"
        static let UserSelfUrl = Instagram.baseUrl + "v1/users/self/"
        static let UserSelfMediaUrl = Instagram.baseUrl + "v1/users/self/media/recent/"
        static let ClientId = "2af7c89e3f284fc9b3bb46f903b2adce"
        static let ClientSecret = "6483de6752b347aebd4418325f211016"
        static let RedirectUri = "http://www.google.com"
        static let AccessToken = "access_token"
        static let Scope = "basic+public_content+follower_list+relationships"
    }
    
    init () {
        userId = KeychainHelper.main[KeychainHelper.userIdKey]
    }
    
    var userId: String!
    
    func isUserLoggedIn() -> Bool {
        return userId != nil
    }
    
    func logout() {
        KeychainHelper.main[KeychainHelper.accessTokenKey] = nil
        KeychainHelper.main[KeychainHelper.userIdKey] = nil
        userId = nil
        clearCookies()
        
        guard let window = UIApplication.shared.keyWindow else { return }
        guard let rootViewController = window.rootViewController else { return }
        
        guard let viewController = R.storyboard.authentication().instantiateInitialViewController() else { return }
        viewController.view.frame = rootViewController.view.frame
        viewController.view.layoutIfNeeded()
        
        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
            window.rootViewController = viewController
        }, completion: nil)
    }
    
    func clearCookies() {
        let storage = HTTPCookieStorage.shared
        storage.cookies?.forEach({ cookie in
            storage.deleteCookie(cookie)
        })
        UserDefaults.standard.synchronize()
        let dataStore = WKWebsiteDataStore.default()
        dataStore.fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { (records) in
            for record in records {
                if record.displayName.contains("instagram") {
                    dataStore.removeData(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes(), for: [record], completionHandler: {})
                }
            }
        }
    }
    
    func login(currentViewController: UIViewController) {
        let loginViewController = UINavigationController(rootViewController: LoginViewController())
        currentViewController.present(loginViewController, animated: true, completion: nil)
    }
    
    struct LoginResponse: Wrapper {
        var accessToken: String!
        var user: User!
        init () {}
        mutating func map(_ map: Map) {
            accessToken <- map["access_token"]
            user <- map["user"]
        }
    }
    
    func loginWithCode(code: String, success: (() -> Void)?, failure: (() -> Void)?) {
        let parameters = ["client_id": Instagram.ClientId,
                          "client_secret": Instagram.ClientSecret,
                          "grant_type": "authorization_code",
                          "redirect_uri": Instagram.RedirectUri,
                          "code": code]
        Alamofire.request(Instagram.AccessTokenUrl, method: .post, parameters: parameters, encoding: URLEncoding.default)
            .responseInsert(context: CoreDataStack.context, type: LoginResponse.self) { response in
                switch response.result {
                case let .success(response):
                    KeychainHelper.main[KeychainHelper.accessTokenKey] = response.accessToken
                    KeychainHelper.main[KeychainHelper.userIdKey] = response.user.id
                    self.userId = String(response.user.id!)
                    success?()
                case .failure:
                    failure?()
                }
        }
    }
    
    struct UserSelfResponse: Wrapper {
        var user: ProfileUser!
        init () {}
        mutating func map(_ map: Map) {
            user <- map["data"]
        }
    }
    
    func getUserInfo(completed: ((Error?) -> Void)?) {
        guard let accessToken = KeychainHelper.main[KeychainHelper.accessTokenKey] else {
            logout()
            return
        }
        let parameters = ["access_token": accessToken]
        Alamofire.request(Instagram.UserSelfUrl, method: .get, parameters: parameters, encoding: URLEncoding.default)
            .responseInsert(context: CoreDataStack.context, type: UserSelfResponse.self) { response in
                switch response.result {
                case .success:
                    completed?(nil)
                case .failure:
                    completed?(response.error)
                }
        }
    }
    
    struct UserMediasResponse: Wrapper {
        var medias: Many<Media>!
        init () {}
        mutating func map(_ map: Map) {
            medias <- map["data"]
        }
    }
    
    func getUserMedias(completed: ((Error?) -> Void)?) {
        guard let accessToken = KeychainHelper.main[KeychainHelper.accessTokenKey] else {
            logout()
            return
        }
        let parameters = ["access_token": accessToken]
        Alamofire.request(Instagram.UserSelfMediaUrl, method: .get, parameters: parameters, encoding: URLEncoding.default)
            .responseInsert(context: CoreDataStack.context, type: UserMediasResponse.self) { response in
                switch response.result {
                case .success:
                    completed?(nil)
                case .failure:
                    completed?(response.error)
                }
        }
    }
}
