//
//  UserProvider.swift
//  InstaLikeApp
//
//  Created by Bonnet Nicolas on 22.10.18.
//  Copyright © 2018 NicolasBonnet. All rights reserved.
//

import CoreData
import KeychainAccess

protocol UserProviderDelegate: NSObjectProtocol  {
    func performUpdatesForUserCoreDataChange(animated: Bool)
}

final class UserProvider: NSObject {
    let userId: String
    var delegate: UserProviderDelegate!
    
    private lazy var userFetchResultController: NSFetchedResultsController<ProfileUser> = {
        let fetchRequest: NSFetchRequest<ProfileUser> = NSFetchRequest(entityName: "ProfileUsers")
        fetchRequest.predicate = NSPredicate(format: "id = %@", userId)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "id", ascending: false)]
        fetchRequest.returnsDistinctResults = true
        let fetchResultController = NSFetchedResultsController(
            fetchRequest: fetchRequest,
            managedObjectContext: CoreDataStack.context,
            sectionNameKeyPath: nil,
            cacheName: nil)
        fetchResultController.delegate = self
        return fetchResultController
    }()
    
    init(aUserId: String) {
        userId = aUserId
        super.init()
        do {
            try userFetchResultController.performFetch()
        }
        catch {
            fatalError("Cannot Fetch! \(error)")
        }
    }
    
    func getUser() -> ProfileUser! {
        let objects = self.userFetchResultController.fetchedObjects
        return objects?.first
    }
}

extension UserProvider: NSFetchedResultsControllerDelegate {
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.delegate?.performUpdatesForUserCoreDataChange(animated: true)
    }
}
