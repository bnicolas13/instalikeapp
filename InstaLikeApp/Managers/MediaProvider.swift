//
//  MediaProvider.swift
//  InstaLikeApp
//
//  Created by Bonnet Nicolas on 21.10.18.
//  Copyright © 2018 NicolasBonnet. All rights reserved.
//

import CoreData
import KeychainAccess

protocol MediaProviderDelegate: NSObjectProtocol  {
    func performUpdatesForMediaCoreDataChange(animated: Bool)
}

final class MediaProvider: NSObject {
    var userId: String!
    var delegate: MediaProviderDelegate!
    
    private lazy var mediaFetchResultController: NSFetchedResultsController<Media> = {
        let fetchRequest: NSFetchRequest<Media> = NSFetchRequest(entityName: "Medias")
        fetchRequest.predicate = NSPredicate(format: "user.id = %@", userId!)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "createdTime", ascending: false)]
        fetchRequest.returnsDistinctResults = true
        let fetchResultController = NSFetchedResultsController(
            fetchRequest: fetchRequest,
            managedObjectContext: CoreDataStack.context,
            sectionNameKeyPath: nil,
            cacheName: nil)
        fetchResultController.delegate = self
        return fetchResultController
    }()
    
    init(aUserId: String) {
        super.init()
        userId = aUserId
        do {
            try mediaFetchResultController.performFetch()
        }
        catch {
            fatalError("Cannot Fetch! \(error)")
        }
    }
    
    func getMediasSection() -> MediasSection {
        let medias = self.mediaFetchResultController.fetchedObjects
        var uniqueMedias: [Media] = []
        var mediaIds: [String] = []
        medias?.forEach({ media in
            if media.id != nil && !mediaIds.contains(media.id!) {
                uniqueMedias.append(media)
                mediaIds.append(media.id!)
            }
        })
        return MediasSection(uniqueMedias)
    }
}

extension MediaProvider: NSFetchedResultsControllerDelegate {
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.delegate?.performUpdatesForMediaCoreDataChange(animated: true)
    }
}
