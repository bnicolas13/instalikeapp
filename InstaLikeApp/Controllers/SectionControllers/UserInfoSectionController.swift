//
//  UserInfoSectionController.swift
//  InstaLikeApp
//
//  Created by Bonnet Nicolas on 16.10.18.
//  Copyright © 2018 NicolasBonnet. All rights reserved.
//

import IGListKit

class UserInfoSectionController: ListSectionController {
    static let sectionHeight: CGFloat = 120
    let userInfoSection: UserInfoSection
    
    init(userInfo: UserInfoSection) {
        userInfoSection = userInfo
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        return CGSize(width: collectionContext!.containerSize.width, height: UserInfoSectionController.sectionHeight)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell = collectionContext!.dequeueReusableCell(withNibName: "UserInfoCollectionViewCell", bundle: nil, for: self, at: index) as? UserInfoCollectionViewCell else {fatalError()}
        cell.updateUI(userInfo: userInfoSection)
        return cell
    }
}
