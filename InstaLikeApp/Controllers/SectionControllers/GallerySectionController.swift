//
//  GallerySectionController.swift
//  InstaLikeApp
//
//  Created by Bonnet Nicolas on 21.10.18.
//  Copyright © 2018 NicolasBonnet. All rights reserved.
//

import IGListKit

class GallerySectionController: ListSectionController {
    
    let mediasSection: MediasSection
    
    init(section: MediasSection) {
        mediasSection = section
        super.init()
        minimumLineSpacing = 4
        minimumInteritemSpacing = 4
        inset = UIEdgeInsets(top: 0, left: 0, bottom: 4, right: 0)
    }
    
    override func numberOfItems() -> Int {
        return mediasSection.medias?.count ?? 0
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        let width = trunc((collectionContext!.containerSize.width - collectionContext!.adjustedContainerInset.left - collectionContext!.adjustedContainerInset.right - 8) / 3)
        return CGSize(width: width, height: width)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: ImageCollectionViewCell.self, for: self, at: index) as! ImageCollectionViewCell
        cell.updateUI(media: mediasSection.medias[index], quality: .low)
        return cell
    }
    
    override func didSelectItem(at index: Int) {
        collectionContext!.deselectItem(at: index, sectionController: self, animated: true)
        guard let viewController = self.viewController as? ProfileViewController else { return }
        viewController.showFullScreen(index: IndexPath(row: index, section: section), medias: mediasSection.medias)
    }
}
