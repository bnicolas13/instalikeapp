//
//  TransitionController.swift
//  InstaLikeApp
//
//  Created by Bonnet Nicolas on 23.10.18.
//  Copyright © 2018 NicolasBonnet. All rights reserved.
//

import UIKit

public struct TransitionInfo {
    var initialIndexPath: IndexPath
    var destinationIndexPath: IndexPath
    init(initialIndexPath: IndexPath, destinationIndexPath: IndexPath) {
        self.initialIndexPath = initialIndexPath
        self.destinationIndexPath = destinationIndexPath
    }
}

public final class TransitionController: NSObject {
    
    public var userInfo: TransitionInfo? = nil
    
    public lazy var presentAnimationController: PresentAnimationController = {
        let controller: PresentAnimationController = PresentAnimationController()
        controller.transitionController = self
        return controller
    }()
    
    public lazy var dismissAnimationController: DismissAnimationController = {
        let controller: DismissAnimationController = DismissAnimationController()
        controller.transitionController = self
        return controller
    }()
    
    public lazy var dismissInteractiveTransition: DismissInteractiveTransition = {
        let interactiveTransition: DismissInteractiveTransition = DismissInteractiveTransition()
        interactiveTransition.transitionController = self
        interactiveTransition.animationController = self.dismissAnimationController
        return interactiveTransition
    }()
    
    fileprivate(set) weak var presentingViewController: UIViewController!
    
    fileprivate(set) var presentedViewController: UIViewController!
    
    public func present<T: ViewTransitionPresented, U: ViewTransitionPresenting>(to presentedViewController: T, on presentingViewController: U, completion: (() -> Void)?) where T: UIViewController, U: UIViewController {
        
        self.presentingViewController = presentingViewController
        self.presentedViewController = presentedViewController
        
        presentingViewController.present(presentedViewController, animated: true, completion: completion)
    }
}

extension TransitionController: UIViewControllerTransitioningDelegate {
    
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        presentAnimationController.prepare()
        return presentAnimationController
    }
    
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        dismissAnimationController.prepare()
        return dismissAnimationController
    }
    
    public func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return dismissInteractiveTransition.interactionInProgress ? dismissInteractiveTransition : nil
    }
}
