//
//  DismissAnimationController.swift
//  InstaLikeApp
//
//  Created by Bonnet Nicolas on 23.10.18.
//  Copyright © 2018 NicolasBonnet. All rights reserved.
//

import UIKit

public final class DismissAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    
    public override init() {
        super.init()
        
        destinationTransitionView = UIImageView(image: nil)
        destinationTransitionView.clipsToBounds = true
        destinationTransitionView.contentMode = .scaleAspectFill
    }
    
    private struct Transition {
        static let transitionDuration: TimeInterval = 0.5
        static let usingSpringWithDamping: CGFloat = 0.9
        static let initialSpringVelocity: CGFloat = 0.0
        static let animationOptions: UIView.AnimationOptions = [.curveEaseInOut, .allowUserInteraction]
        static let usingSpringWithDampingCancelling: CGFloat = 1.0
        static let initialSpringVelocityCancelling: CGFloat = 0.0
        static let animationOptionsCancelling: UIView.AnimationOptions = [.curveEaseInOut, .allowUserInteraction]
    }
    
    public weak var transitionController: TransitionController!
    fileprivate(set) var initialView: UIView!
    fileprivate(set) var destinationView: UIView!
    fileprivate(set) var initialFrame: CGRect!
    fileprivate(set) var destinationFrame: CGRect!
    fileprivate(set) var destinationTransitionView: UIImageView!
    
    var initialSnapshotImage: UIImage?
    var destinationSnapshotImage: UIImage?
    
    func prepare() {
        guard let presentingViewController: ViewTransitionPresenting = transitionController.presentingViewController as? ViewTransitionPresenting else { return }
        guard let presentedViewController: ViewTransitionPresented = transitionController.presentedViewController as? ViewTransitionPresented else { return }
        
        presentingViewController.prepareInitialView(transitionController.userInfo, isPresenting: false)
        let initialView: UIView = presentingViewController.initialView(transitionController.userInfo, isPresenting: false)
        initialSnapshotImage = initialView.snapshotImage()
        
        presentedViewController.prepareDestinationView(transitionController.userInfo, isPresenting: false)
        let destinationView: UIView = presentedViewController.destinationView(transitionController.userInfo, isPresenting: false)
        destinationSnapshotImage = destinationView.snapshotImage()
    }
    
    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return Transition.transitionDuration
    }
    
    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        var tempFromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)
        if tempFromViewController is UINavigationController {
            tempFromViewController = (tempFromViewController as! UINavigationController).topViewController
        }
        guard let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from) as? ViewTransitionPresented , fromViewController is UIViewController else { return }
        var tempToViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
        if tempToViewController is UINavigationController {
            tempToViewController = (tempToViewController as! UINavigationController).topViewController
        }
        guard let toViewController = tempToViewController as? ViewTransitionPresenting , toViewController is UIViewController else { return }
        
        let containerView = transitionContext.containerView
        containerView.backgroundColor = UIColor.clear
        
        fromViewController.prepareDestinationView(self.transitionController.userInfo, isPresenting: false)
        destinationView = fromViewController.destinationView(self.transitionController.userInfo, isPresenting: false)
        destinationFrame = fromViewController.destinationFrame(self.transitionController.userInfo, isPresenting: false)
        
        toViewController.prepareInitialView(self.transitionController.userInfo, isPresenting: false)
        initialView = toViewController.initialView(self.transitionController.userInfo, isPresenting: false)
        initialFrame = toViewController.initialFrame(self.transitionController.userInfo, isPresenting: false)
        
        // Hide Transisioning Views
        initialView.isHidden = true
        destinationView.isHidden = true
        
        // Add To,FromViewController's View
        let toViewControllerView: UIView = (toViewController as! UIViewController).view
        if (toViewController as! UIViewController).navigationController != nil {
            containerView.addSubview((toViewController as! UIViewController).navigationController!.view)
        }
        let fromViewControllerView: UIView = (fromViewController as! UIViewController).view
        containerView.addSubview(fromViewControllerView)
        
        // This condition is to prevent getting white screen at dismissing when multiple view controller are presented.
        let isNeedToControlToViewController: Bool = toViewControllerView.superview == nil
        if isNeedToControlToViewController {
            containerView.addSubview(toViewControllerView)
            containerView.sendSubviewToBack(toViewControllerView)
        }
        
        // Add Snapshot
        destinationTransitionView.transform = .identity
        destinationTransitionView.image = destinationSnapshotImage
        destinationTransitionView.frame = destinationFrame
        containerView.addSubview(destinationTransitionView)
        
        // Animation
        let duration: TimeInterval = transitionDuration(using: transitionContext)
        
        if transitionContext.isInteractive {
            UIView.animate(withDuration: duration, delay: 0.0, usingSpringWithDamping: Transition.usingSpringWithDampingCancelling, initialSpringVelocity: Transition.initialSpringVelocityCancelling, options: Transition.animationOptionsCancelling, animations: {
                fromViewControllerView.alpha = CGFloat.leastNormalMagnitude
            }, completion: nil)
        } else {
            UIView.animate(withDuration: duration, delay: 0.0, usingSpringWithDamping: Transition.usingSpringWithDamping, initialSpringVelocity: Transition.initialSpringVelocity, options: Transition.animationOptions, animations: {
                
                self.destinationTransitionView.frame = self.initialFrame
                fromViewControllerView.alpha = CGFloat.leastNormalMagnitude
                
            }, completion: { _ in
                // Clean transition context
                self.destinationTransitionView.removeFromSuperview()
                if isNeedToControlToViewController {
                    toViewControllerView.removeFromSuperview()
                }
                self.initialView.isHidden = false
                self.destinationView.isHidden = false
                
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            })
        }
    }
}
