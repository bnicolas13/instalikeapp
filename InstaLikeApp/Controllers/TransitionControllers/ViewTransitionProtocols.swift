//
//  ViewTransitionProtocols.swift
//  InstaLikeApp
//
//  Created by Bonnet Nicolas on 23.10.18.
//  Copyright © 2018 NicolasBonnet. All rights reserved.
//

import UIKit

public protocol ViewTransitionPresenting {
    func initialFrame(_ userInfo: TransitionInfo?, isPresenting: Bool) -> CGRect
    func initialView(_ userInfo: TransitionInfo?, isPresenting: Bool) -> UIView
    func prepareInitialView(_ userInfo: TransitionInfo?, isPresenting: Bool) -> Void
}

public protocol ViewTransitionPresented {
    func destinationFrame(_ userInfo: TransitionInfo?, isPresenting: Bool) -> CGRect
    func destinationView(_ userInfo: TransitionInfo?, isPresenting: Bool) -> UIView
    func prepareDestinationView(_ userInfo: TransitionInfo?, isPresenting: Bool) -> Void
}

