//
//  PresentAnimationController.swift
//  InstaLikeApp
//
//  Created by Bonnet Nicolas on 23.10.18.
//  Copyright © 2018 NicolasBonnet. All rights reserved.
//

import UIKit

public final class PresentAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    
    public override init() {
        super.init()
        
        initialTransitionView = UIImageView()
        initialTransitionView.clipsToBounds = true
        initialTransitionView.contentMode = .scaleAspectFill
    }
    
    struct Transition {
        static let transitionDuration: TimeInterval = 0.5
        static let usingSpringWithDamping: CGFloat = 0.9
        static let initialSpringVelocity: CGFloat = 0.0
        static let animationOptions: UIView.AnimationOptions = [.curveEaseInOut, .allowUserInteraction]
    }
    
    public weak var transitionController: TransitionController!
    fileprivate(set) var initialTransitionView: UIImageView!
    fileprivate(set) var destinationTransitionView: UIImageView!
    var initialSnapshotImage: UIImage?
    var destinationSnapshotImage: UIImage?
    
    func prepare() {
        guard let presentingViewController = transitionController.presentingViewController as? ViewTransitionPresenting else { return }
        guard let presentedViewController = transitionController.presentedViewController as? ViewTransitionPresented else { return }
        
        presentingViewController.prepareInitialView(transitionController.userInfo, isPresenting: true)
        let initialView: UIView = presentingViewController.initialView(transitionController.userInfo, isPresenting: true)
        initialSnapshotImage = initialView.snapshotImage()
        
        presentedViewController.prepareDestinationView(transitionController.userInfo, isPresenting: true)
        let destinationView: UIView = presentedViewController.destinationView(transitionController.userInfo, isPresenting: true)
        destinationSnapshotImage = destinationView.snapshotImage()
    }
    
    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return Transition.transitionDuration
    }
    
    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        var tempFromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)
        if tempFromViewController is UINavigationController {
            tempFromViewController = (tempFromViewController as! UINavigationController).topViewController
        }
        guard let fromViewController = tempFromViewController as? ViewTransitionPresenting, fromViewController is UIViewController else { return }
        var tempToViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
        if tempToViewController is UINavigationController {
            tempToViewController = (tempToViewController as! UINavigationController).topViewController
        }
        guard let toViewController = tempToViewController as? ViewTransitionPresented , toViewController is UIViewController else { return }
        
        let containerView = transitionContext.containerView
        
        fromViewController.prepareInitialView(self.transitionController.userInfo, isPresenting: true)
        let initialView: UIView = fromViewController.initialView(self.transitionController.userInfo, isPresenting: true)
        let initialFrame: CGRect = fromViewController.initialFrame(self.transitionController.userInfo, isPresenting: true)
        
        toViewController.prepareDestinationView(self.transitionController.userInfo, isPresenting: true)
        let destinationView: UIView = toViewController.destinationView(self.transitionController.userInfo, isPresenting: true)
        let destinationFrame: CGRect = toViewController.destinationFrame(self.transitionController.userInfo, isPresenting: true)
        
        // Hide Transisioning Views
        initialView.isHidden = true
        destinationView.isHidden = true
        
        // Add ToViewController's View
        let toViewControllerView: UIView = (toViewController as! UIViewController).view
        toViewControllerView.alpha = CGFloat.leastNormalMagnitude
        containerView.addSubview(toViewControllerView)
        
        // Add Snapshot
        initialTransitionView.image = initialSnapshotImage
        initialTransitionView.frame = initialFrame
        containerView.addSubview(initialTransitionView)
        
        // Animation
        let duration: TimeInterval = transitionDuration(using: transitionContext)
        UIView.animate(withDuration: duration, delay: 0.0, usingSpringWithDamping: Transition.usingSpringWithDamping, initialSpringVelocity: Transition.initialSpringVelocity, options: Transition.animationOptions, animations: {

            self.initialTransitionView.frame = destinationFrame
            toViewControllerView.alpha = 1.0
            
        }, completion: { _ in
            // Clean transition context
            self.initialTransitionView.removeFromSuperview()
            initialView.isHidden = false
            destinationView.isHidden = false
            
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }
}
