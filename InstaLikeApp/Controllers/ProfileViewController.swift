//
//  ProfileViewController.swift
//  InstaLikeApp
//
//  Created by Bonnet Nicolas on 16.10.18.
//  Copyright © 2018 NicolasBonnet. All rights reserved.
//

import UIKit
import IGListKit

class ProfileViewController: UIViewController, UserManagerInjector {
    var mediaProvider: MediaProvider!
    var userProvider: UserProvider!
    var isTitleImageHidden = true
    var numberOfLoading: Int = 0
    
    @IBOutlet weak var collectionView: UICollectionView!
    fileprivate let titleView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = 8
        return stackView
    }()
    fileprivate let titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        return label
    }()
    fileprivate let titleimageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = 15
        imageView.layer.borderColor = UIColor.black.withAlphaComponent(0.3).cgColor
        imageView.layer.borderWidth = 0.5
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.addConstraints( NSLayoutConstraint.constraints(withVisualFormat: "V:[imageView(30)]", options: NSLayoutConstraint.FormatOptions.alignAllCenterY, metrics: nil, views: ["imageView": imageView]))
        imageView.addConstraints( NSLayoutConstraint.constraints(withVisualFormat: "H:[imageView(30)]", options: NSLayoutConstraint.FormatOptions.alignAllCenterX, metrics: nil, views: ["imageView": imageView]))
        imageView.alpha = 0
        imageView.isHidden = true
        return imageView
    }()
    lazy var refreshButton: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh,
                                                              target: self,
                                                              action: #selector(ProfileViewController.onRefresh))
    let activityIndicatorBarButton: UIBarButtonItem = {
        let indicator = UIActivityIndicatorView(style: .gray)
        indicator.startAnimating()
        let view = UIBarButtonItem(customView: indicator)
        return view
    } ()
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self,
                                 action: #selector(ProfileViewController.onRefresh),
                                 for: UIControl.Event.valueChanged)
        refreshControl.layer.zPosition = -1
        return refreshControl
    }()
    
    lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self)
    }()
    let transitionController: TransitionController = TransitionController()
    
    func data() -> [ListDiffable] {
        return [UserInfoSection(id: self.userManager.userId, user: userProvider?.getUser()), mediaProvider?.getMediasSection() ?? MediasSection(nil)]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adapter.collectionView = collectionView
        adapter.dataSource = self
        adapter.scrollViewDelegate = self
        mediaProvider = MediaProvider(aUserId: (self.userManager.userId)!)
        mediaProvider.delegate = self
        userProvider = UserProvider(aUserId: (self.userManager.userId)!)
        userProvider.delegate = self
        if #available(iOS 11.0, *) {
            collectionView.contentInsetAdjustmentBehavior = .always
        }
        collectionView.addSubview(refreshControl)
        titleView.addArrangedSubview(titleLabel)
        navigationItem.titleView = titleView
        navigationItem.rightBarButtonItem = refreshButton
        navigationController?.navigationBar.tintColor = UIColor.black
        updateUI()
        loadData()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: nil) { (_) in
        }
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    func loadData() {
        numberOfLoading += 2
        navigationItem.rightBarButtonItem = activityIndicatorBarButton
        self.userManager.getUserInfo { error in
            self.updateLoading()
        }
        self.userManager.getUserMedias { error in
            self.updateLoading()
        }
    }
    
    func updateLoading() {
        numberOfLoading -= 1
        if numberOfLoading == 0 {
            navigationItem.rightBarButtonItem = refreshButton
        }
    }
    
    func updateUI() {
        adapter.performUpdates(animated: true)
        titleLabel.text = userProvider.getUser()?.username
        if userProvider.getUser()?.profilePicture != nil {
            titleimageView.sd_setImage(with: URL(string: (userProvider.getUser()?.profilePicture)!), completed: nil)
        }
    }
    
    @objc func onRefresh() {
        refreshControl.endRefreshing()
        loadData()
    }
}

extension ProfileViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if isTitleImageHidden != (scrollView.contentOffset.y < UserInfoSectionController.sectionHeight) {
            isTitleImageHidden = scrollView.contentOffset.y < UserInfoSectionController.sectionHeight
            if isTitleImageHidden {
                UIView.animate(withDuration: 0.3, animations: {
                    self.titleimageView.alpha = 0
                    self.titleimageView.isHidden = true
                }, completion: { _ in
                    self.titleimageView.removeFromSuperview()
                })
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.titleimageView.alpha = 1
                    self.titleView.insertArrangedSubview(self.titleimageView, at: 0)
                    self.titleimageView.isHidden = false
                })
            }
        }
    }
}

extension ProfileViewController: ListAdapterDataSource {
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return data()
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        switch object {
        case is UserInfoSection: return UserInfoSectionController(userInfo: object as! UserInfoSection)
        case is MediasSection: return GallerySectionController(section: object as! MediasSection)
        default: return ListSectionController()
        }
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? { return nil }
}

extension ProfileViewController: MediaProviderDelegate {
    func performUpdatesForMediaCoreDataChange(animated: Bool) {
        updateUI()
    }
}

extension ProfileViewController: UserProviderDelegate {
    func performUpdatesForUserCoreDataChange(animated: Bool) {
        updateUI()
    }
}

extension ProfileViewController: ViewTransitionPresenting {
    func showFullScreen (index: IndexPath, medias: [Media]) {
        let presentedViewController: FullScreenGalleryViewController = FullScreenGalleryViewController(medias: medias)
        presentedViewController.transitionController = transitionController
        transitionController.userInfo = TransitionInfo(initialIndexPath: index, destinationIndexPath: IndexPath(row: index.row, section: 0))
        presentedViewController.transitioningDelegate = transitionController
        transitionController.present(to: presentedViewController, on: self, completion: nil)
    }
    
    func initialFrame(_ userInfo: TransitionInfo?, isPresenting: Bool) -> CGRect {
        guard let indexPath: IndexPath = userInfo?.initialIndexPath, let attributes: UICollectionViewLayoutAttributes = self.collectionView.layoutAttributesForItem(at: indexPath) else {
            return CGRect.zero
        }
        return self.collectionView.convert(attributes.frame, to: (UIApplication.shared.delegate as? AppDelegate)?.window)
    }
    
    func initialView(_ userInfo: TransitionInfo?, isPresenting: Bool) -> UIView {
        let indexPath: IndexPath = userInfo!.initialIndexPath
        guard let cell: UICollectionViewCell = self.collectionView.cellForItem(at: indexPath) else { return UIView() }
        return cell.contentView
    }
    
    func prepareInitialView(_ userInfo: TransitionInfo?, isPresenting: Bool) {
        let indexPath: IndexPath = userInfo!.initialIndexPath
        if !self.collectionView.indexPathsForVisibleItems.contains(indexPath) {
            self.collectionView.scrollToItem(at: indexPath, at: .centeredVertically, animated: false)
        }
    }
}
