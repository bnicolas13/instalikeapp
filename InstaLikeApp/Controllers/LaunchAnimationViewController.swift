//
//  LaunchAnimationViewController.swift
//  InstaLikeApp
//
//  Created by Bonnet Nicolas on 25.10.18.
//  Copyright © 2018 NicolasBonnet. All rights reserved.
//

import UIKit

class LaunchAnimationViewController: UIViewController {
    
    @IBOutlet weak var logoStartImageView: UIImageView!
    @IBOutlet weak var mainEndView: UIView!
    @IBOutlet weak var logoEndImageView: UIImageView!
    @IBOutlet weak var fancyView: FancyView!
    @IBOutlet weak var loginButton: UIButton! {
        didSet {
            loginButton.setTitle(R.string.localizable.loginButton(), for: .normal)
            loginButton.layer.borderColor = UIColor.white.withAlphaComponent(0.7).cgColor
            loginButton.layer.borderWidth = 1.0
            loginButton.layer.cornerRadius = 4
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainEndView.alpha = 0
        fancyView.setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let endImageFrame = logoEndImageView.frame
        logoEndImageView.frame = self.logoStartImageView.frame
        UIView.animate(withDuration: 1, animations: {
            self.logoStartImageView.frame = endImageFrame
            self.logoEndImageView.frame = endImageFrame
            self.logoStartImageView.alpha = 0
            self.mainEndView.alpha = 1
        }) { completed in
            guard let window = UIApplication.shared.keyWindow else { return }
            guard let rootViewController = window.rootViewController else { return }
            guard let viewController = R.storyboard.authentication().instantiateInitialViewController() else { return }
            viewController.view.frame = rootViewController.view.frame
            viewController.view.layoutIfNeeded()
            window.rootViewController = viewController
        }
    }
}
