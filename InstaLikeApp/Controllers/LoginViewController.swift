//
//  LoginViewController.swift
//  InstaLikeApp
//
//  Created by Bonnet Nicolas on 15.10.18.
//  Copyright © 2018 NicolasBonnet. All rights reserved.
//

import CoreData
import UIKit
import WebKit

class LoginViewController: UIViewController, UserManagerInjector {
    
    fileprivate let webView: WKWebView = {
        let view = WKWebView()
        return view
    }()
    private var progressView: UIProgressView!
    private var webViewObservation: NSKeyValueObservation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel,
                                                            target: self,
                                                            action: #selector(LoginViewController.onCancel))
        view.addSubview(webView)
        webView.navigationDelegate = self
        setupProgressView()
        webViewObservation = webView.observe(\.estimatedProgress, changeHandler: progressViewChangeHandler)
    }
    
    deinit {
        progressView.removeFromSuperview()
        webViewObservation.invalidate()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        webView.frame = view.bounds
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        title = R.string.localizable.loginInstagramTitle()
        
        let authUrl = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=code&scope=%@&DEBUG=True",
                            UserManager.Instagram.AuthUrl,
                            UserManager.Instagram.ClientId,
                            UserManager.Instagram.RedirectUri,
                            UserManager.Instagram.Scope)
        webView.load(URLRequest(url: URL(string: authUrl)!))
    }
    
    private func setupProgressView() {
        let navBar = navigationController!.navigationBar
        
        progressView = UIProgressView(progressViewStyle: .bar)
        progressView.progress = 0.0
        progressView.tintColor = Colors.Red
        progressView.translatesAutoresizingMaskIntoConstraints = false
        
        navBar.addSubview(progressView)
        
        let bottomConstraint = navBar.bottomAnchor.constraint(equalTo: progressView.bottomAnchor, constant: 1)
        let leadingConstraint = navBar.leadingAnchor.constraint(equalTo: progressView.leadingAnchor)
        let trailingConstraint = navBar.trailingAnchor.constraint(equalTo: progressView.trailingAnchor)
        
        NSLayoutConstraint.activate([bottomConstraint, leadingConstraint, trailingConstraint])
    }
    
    @objc func onCancel() {
        dismiss(animated: true, completion: nil)
    }
    
    private func progressViewChangeHandler<Value>(webView: WKWebView, change: NSKeyValueObservedChange<Value>) {
        progressView.alpha = 1.0
        progressView.setProgress(Float(webView.estimatedProgress), animated: true)
        
        if webView.estimatedProgress >= 1.0 {
            UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseInOut, animations: {
                self.progressView.alpha = 0.0
            }, completion: { _ in
                self.progressView.progress = 0
            })
        }
    }
}

extension LoginViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if checkRequestForCallbackURL(request: navigationAction.request) {
            decisionHandler(.allow)
            return
        }
        decisionHandler(.cancel)
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) { }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) { }
    
    func checkRequestForCallbackURL(request: URLRequest) -> Bool {
        let urlString = request.url?.absoluteString
        if urlString?.hasPrefix(UserManager.Instagram.RedirectUri) ?? false {
            // extract and handle access token
            guard let url = URLComponents(string: urlString!) else { return false }
            guard let code = url.queryItems?.first(where: { $0.name == "code" })?.value else { return false }
            self.userManager.loginWithCode(code: code, success: {
                self.dismiss(animated: true, completion: nil)
            }, failure: nil)
            return false
        }
        return true
    }
    
}
