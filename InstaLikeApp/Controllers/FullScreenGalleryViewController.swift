//
//  FullScreenGalleryViewController.swift
//  InstaLikeApp
//
//  Created by Bonnet Nicolas on 23.10.18.
//  Copyright © 2018 NicolasBonnet. All rights reserved.
//

import UIKit

class FullScreenGalleryViewController: UIViewController, UIGestureRecognizerDelegate {
    
    let imageCollectionViewCellIdentifier: String = "imageCollectionViewCellIdentifier"
    let itemSpacing: CGFloat = 8
    
    let medias: [Media]
    weak var transitionController: TransitionController!
    
    lazy var navigationBar: UINavigationBar = {
        let navigationBar = UINavigationBar()
        navigationBar.translatesAutoresizingMaskIntoConstraints = false
        navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = true
        navigationBar.tintColor = UIColor.white
        return navigationBar
    }()
    
    lazy var collectionView: UICollectionView = {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let collectionView: UICollectionView = UICollectionView(frame: self.view.bounds, collectionViewLayout: layout)
        collectionView.register(ImageCollectionViewCell.self, forCellWithReuseIdentifier: imageCollectionViewCellIdentifier)
        collectionView.backgroundColor = UIColor.black
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.decelerationRate = .fast
        return collectionView
    }()
    
    init(medias: [Media]) {
        self.medias = medias
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(collectionView)
        view.addSubview(navigationBar)
        let standaloneItem = UINavigationItem()
        standaloneItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done,
                                                            target: self,
                                                            action: #selector(FullScreenGalleryViewController.onDone))
        navigationBar.items = [standaloneItem]
        navigationBar.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        navigationBar.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        if #available(iOS 11, *) {
            navigationBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        } else {
            navigationBar.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        }
        
        let pan = UIPanGestureRecognizer(target: self.transitionController.dismissInteractiveTransition, action: #selector(self.transitionController.dismissInteractiveTransition.handlePanGesture(_:)))
        pan.delegate = self
        pan.maximumNumberOfTouches = 1
        view.addGestureRecognizer(pan)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        let index = Int(trunc(collectionView.contentOffset.x / (collectionView.bounds.size.width + itemSpacing)))
        coordinator.animate(alongsideTransition: nil) { (_) in
            self.collectionView.frame = self.view.bounds
            self.collectionView.layoutIfNeeded()
            let contentOfffset: CGPoint = CGPoint(x: (self.collectionView.bounds.size.width + self.itemSpacing) * CGFloat(index), y: 0.0)
            self.collectionView.contentOffset = contentOfffset
            self.collectionView.reloadData()
        }
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    @objc func onDone() {
        self.transitionController.userInfo = transitionInfo()
        self.dismiss(animated: true, completion: nil)
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        self.transitionController.userInfo = transitionInfo()
        let panGestureRecognizer: UIPanGestureRecognizer = gestureRecognizer as! UIPanGestureRecognizer
        let transate: CGPoint = panGestureRecognizer.translation(in: self.view)
        return Double(abs(transate.y)/abs(transate.x)) > .pi / 4.0
    }
    
    private func transitionInfo() -> TransitionInfo {
        let indexPath: IndexPath = self.collectionView.indexPathsForVisibleItems.first!
        return TransitionInfo(initialIndexPath: IndexPath(item: indexPath.row, section: (self.transitionController.userInfo?.initialIndexPath.section)!), destinationIndexPath: indexPath)
    }
}

extension FullScreenGalleryViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width: CGFloat = collectionView.bounds.width
        let height: CGFloat = collectionView.bounds.height
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return itemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return medias.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: imageCollectionViewCellIdentifier, for: indexPath) as! ImageCollectionViewCell
        var imageRatio: CGFloat? = nil
        if let image = medias[indexPath.row].images?.standard {
            imageRatio = CGFloat(image.height) / CGFloat(image.width)
        }
        cell.updateUI(media: medias[indexPath.row], customRatio: imageRatio)
        return cell
    }
}

extension FullScreenGalleryViewController: ViewTransitionPresented {
    func destinationFrame(_ userInfo: TransitionInfo?, isPresenting: Bool) -> CGRect {
        let indexPath: IndexPath = userInfo!.destinationIndexPath
        let cell = self.collectionView.cellForItem(at: indexPath) as! ImageCollectionViewCell
        return cell.convert(cell.imageView.frame, to: view)
    }
    
    func destinationView(_ userInfo: TransitionInfo?, isPresenting: Bool) -> UIView {
        let indexPath: IndexPath = userInfo!.destinationIndexPath
        let cell = self.collectionView.cellForItem(at: indexPath) as! ImageCollectionViewCell
        return cell.imageView
    }
    
    func prepareDestinationView(_ userInfo: TransitionInfo?, isPresenting: Bool) {
        if isPresenting {
            let indexPath: IndexPath = userInfo!.destinationIndexPath
            let contentOfffset: CGPoint = CGPoint(x: (collectionView.bounds.size.width + itemSpacing) * CGFloat(indexPath.item), y: 0.0)
            self.collectionView.contentOffset = contentOfffset
            self.collectionView.reloadData()
            self.collectionView.layoutIfNeeded()
        }
    }
}

extension FullScreenGalleryViewController: UIScrollViewDelegate {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        var index = trunc(collectionView.contentOffset.x / (collectionView.frame.size.width + itemSpacing))
        if velocity.x > 0 {
            index += 1
        }
        targetContentOffset.pointee.x = index * (collectionView.frame.size.width + itemSpacing)
    }
}
