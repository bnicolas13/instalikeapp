//
//  ViewController.swift
//  InstaLikeApp
//
//  Created by Bonnet Nicolas on 14.10.18.
//  Copyright © 2018 NicolasBonnet. All rights reserved.
//

import UIKit
import CoreData

class AuthenticationViewController: UIViewController, UserManagerInjector {

    @IBOutlet weak var backgroundView: FancyView!
    @IBOutlet weak var loginButton: UIButton! {
        didSet {
            loginButton.setTitle(R.string.localizable.loginButton(), for: .normal)
            loginButton.layer.borderColor = UIColor.white.withAlphaComponent(0.7).cgColor
            loginButton.layer.borderWidth = 1.0
            loginButton.layer.cornerRadius = 4
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(animated)
        backgroundView.startAnimation()
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super .viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.userManager.isUserLoggedIn() {
            goToMainView()
        }
    }
    
    @IBAction func login(_ sender: Any) {
        self.userManager.login(currentViewController: self)
    }
    
    func goToMainView() {
        guard let window = UIApplication.shared.keyWindow else { return }
        guard let rootViewController = window.rootViewController else { return }
        
        guard let viewController = R.storyboard.main().instantiateInitialViewController() else { return }
        viewController.view.frame = rootViewController.view.frame
        viewController.view.layoutIfNeeded()
        
        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
            window.rootViewController = viewController
        }, completion: nil)
    }
}

