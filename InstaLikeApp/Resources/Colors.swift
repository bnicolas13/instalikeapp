//
//  Colors.swift
//  InstaLikeApp
//
//  Created by Bonnet Nicolas on 15.10.18.
//  Copyright © 2018 NicolasBonnet. All rights reserved.
//

import UIKit

struct Colors {
    static let BlueLight = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
    static let BlueDark = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
    static let Purple = #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1)
    static let Red = #colorLiteral(red: 0.8784313725, green: 0.1882352941, blue: 0.4196078431, alpha: 1)
    static let Orange = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
    static let Yellow = #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
    static let GreenLight = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
    static let GreenDark = #colorLiteral(red: 0.1450980392, green: 0.6015064898, blue: 0.2627018952, alpha: 1)
}
