//
//  Keychain.swift
//  InstaLikeApp
//
//  Created by Bonnet Nicolas on 17.10.18.
//  Copyright © 2018 NicolasBonnet. All rights reserved.
//

import KeychainAccess

class KeychainHelper {
    static let accessTokenKey = "access_token"
    static let userIdKey = "user_id"
    
    static let main = Keychain(service: "com.swiss.instalikeapp")
}
