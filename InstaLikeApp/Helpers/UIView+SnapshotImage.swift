//
//  UIView+SnapshotImage.swift
//  InstaLikeApp
//
//  Created by Bonnet Nicolas on 23.10.18.
//  Copyright © 2018 NicolasBonnet. All rights reserved.
//

import UIKit

public extension UIView {
    
    public func snapshotImage() -> UIImage? {
        let size: CGSize = CGSize(width: floor(frame.size.width), height: floor(frame.size.height))
        UIGraphicsBeginImageContextWithOptions(size, true, 0.0)
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        layer.render(in: context)
        let snapshot: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return snapshot
    }
}
