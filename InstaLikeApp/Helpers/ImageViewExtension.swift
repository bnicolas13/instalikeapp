//
//  ImageViewExtension.swift
//  InstaLikeApp
//
//  Created by Bonnet Nicolas on 22.10.18.
//  Copyright © 2018 NicolasBonnet. All rights reserved.
//

import SDWebImage

enum ImageQuality {
    case thumbnail
    case low
    case original
}

extension UIImageView {
    func setImageWith(imageBlock: ImageBlock?, quality: ImageQuality = .original, usingCache: Bool = true, completed:  ((UIImage?) -> Void)? = nil) {
        guard imageBlock != nil else { return }
        let currentImage = self.image
        
        var url: URL?
        switch quality {
        case .original:
            if imageBlock!.standard != nil {
                url = URL(string: imageBlock!.standard!.url!)
            } else {
                return setImageWith(imageBlock: imageBlock, quality: .low, usingCache: usingCache, completed: completed)
            }
        case .low:
            if imageBlock!.low != nil {
                url = URL(string: imageBlock!.low!.url!)
            } else {
                return setImageWith(imageBlock: imageBlock, quality: .thumbnail, usingCache: usingCache, completed: completed)
            }
        case .thumbnail:
            if imageBlock!.thumbnail != nil {
                url = URL(string: imageBlock!.thumbnail!.url!)
            } else {
                return
            }
        }
        // This allows to show a lower quality image while loading an heavier image
        if (imageBlock!.low != nil && quality == .original) {
            let lowUrl = URL(string: imageBlock!.low!.url!)
            SDWebImageManager.shared().cachedImageExists(for: lowUrl) { exist in
                if exist {
                    self.sd_setImage(with: lowUrl, placeholderImage: currentImage, options: .fromCacheOnly, completed: nil)
                }
            }
        }
        if url == nil {
            return
        }
        self.sd_setImage(with: url!, placeholderImage: currentImage, options: usingCache ? [] : [.refreshCached, .retryFailed], completed:  { (image, error, cacheType, url) in
            if error != nil {
                completed?(image)
            }
        })
        
    }
}
